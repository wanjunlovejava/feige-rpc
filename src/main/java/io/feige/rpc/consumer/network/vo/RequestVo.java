package io.feige.rpc.consumer.network.vo;

import io.feige.rpc.consumer.network.future.InvokeFuture;

public class RequestVo {

	private Object request;
	
	private InvokeFuture<Object> future;

	public Object getRequest() {
		return request;
	}

	public void setRequest(Object request) {
		this.request = request;
	}

	public InvokeFuture<Object> getFuture() {
		return future;
	}

	public void setFuture(InvokeFuture<Object> future) {
		this.future = future;
	}
	
}
