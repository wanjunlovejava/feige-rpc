package io.feige.rpc.producer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ExportedServiceFactory {
	
	private static Map<String, Object> serviceMap=new ConcurrentHashMap<String, Object>();
	
	public static void addService(Class<?> clazz, Object service){
		serviceMap.put(clazz.getName(), service);
	}
	
	public static Object getService(String interfaceName){
		return serviceMap.get(interfaceName);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getService(Class<T> clazz){
		return (T)getService(clazz.getName());
	}
	
}
